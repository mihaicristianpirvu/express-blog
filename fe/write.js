import {fetchGetDBById, fetchUpdateById} from "./api_calls.js"
import {getURLParams} from "./libs/urlparams.js"

console.log("[write.js] Client-side code running - write/edit new blogpost")

/* easy mde stuff */
const textArea = document.getElementById("my-text-area")
const easyMDEOptions = {
    element: textArea,
    spellChecker: false,
    sideBySideFullscreen: true,
    previewRender: (plainText) => {
        /* this painful code is required becaue MathJax only updates the rendered DOM globally */
        setTimeout(() => {
            MathJax.typeset()
        }, 1000);
        return plainText;
    },
}
const easyMDE = new EasyMDE(easyMDEOptions)

/* global variables for mde updates */
const toolbar = document.getElementsByClassName("editor-toolbar")[0]
const newDiv = document.createElement("div")
const titleMsgSpan = document.createElement("span")
const titleInput = document.createElement("input")
const outMessageDiv = document.createElement("div")
const publishButton = document.createElement("input")

var blogPostId = getIdFromURL()
console.log(`[write.js] Blog post id: ${blogPostId}`)

function getIdFromURL() {
    const queryParams = getURLParams()
    if (!("id" in queryParams)) {
        return null
    }

    return queryParams["id"]
}

function updateEasyMdeStuff() {
    /* add a new input text and a span to mark the title of the blog post */
    toolbar.style = "display: flex; align-items: center;"
    newDiv.style = "width: 100%"
    titleMsgSpan.innerHTML = "Title"
    titleInput.style = "margin-left: 1em"
    titleInput.setAttribute("type", "text")
    outMessageDiv.style = "float: right"
    publishButton.setAttribute("type", "button")
    publishButton.value = "Publish!"
    publishButton.addEventListener("click", (e) => fetchUpdateById(blogPostId, titleInput.value,
                                                                   easyMDE.value(), handleResultAndRedirect))
    newDiv.appendChild(titleMsgSpan)
    newDiv.appendChild(titleInput)
    newDiv.appendChild(publishButton)
    newDiv.appendChild(outMessageDiv)
    toolbar.appendChild(newDiv)
}

/* handles an update from db (adds a message for 5s to tell us it updated the blog post) */
function handleResultUpdate(data) {
    outMessageDiv.innerHTML = `Saved. Blog post id: ${data["id"]}`
    setInterval(function() {
        outMessageDiv.innerHTML = ""
    }, 5000)

    if(!blogPostId) {
        blogPostId = data["id"]
    }
    if(blogPostId != data["id"]) {
        throw Error(`Different blog post ids. Read from URL: ${blogPostId}. Received: ${data["id"]}`)
    }
}

function handleResultAndRedirect(data) {
    handleResultUpdate(data)
    window.location.href = `read?id=${blogPostId}`
}

function saveInDBOnSomeKeys() {
    document.addEventListener("keydown", (e) => {
        /* ctrl + s */
        if (e.ctrlKey && e.keyCode == 83) {
            e.preventDefault()
            fetchUpdateById(blogPostId, titleInput.value, easyMDE.value(), handleResultUpdate)
        }
        /* ctrl + enter */
        if (e.ctrlKey && e.keyCode == 13) {
            e.preventDefault()
            fetchUpdateById(blogPostId, titleInput.value, easyMDE.value(), handleResultAndRedirect)
        }
    })
}

/* handles reads to the database */
function handleResultGet(data) {
    titleInput.value = data["title"]
    easyMDE.value(data["content"])
    MathJax.typeset()
}

/* Main function of this code. */
(function() {
    easyMDE.toggleSideBySide()
    easyMDE.codemirror.on("change", () => {
        // setTimeout(() => {MathJax.typeset();}, 0)
    })

    updateEasyMdeStuff()
    saveInDBOnSomeKeys()
    /* update text to the markdown editor only if we have an id */
    if (blogPostId){
        fetchGetDBById(blogPostId, handleResultGet)
    }
})()
