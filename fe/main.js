import { fetchGetAllTitles } from "./api_calls.js"

console.log("Client-side code running - blog post list")
const blogListUl = document.getElementById("blogList")

function handleResultGetAll(data) {
  console.log(data[0].title)
  for (var i=0; i<data.length; i++) {
    const newLi = document.createElement("li")
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false
    };
  const ds = (new Date(data[i].timestamp)).toLocaleDateString("en-GB", options)

    newLi.innerHTML = `<a href="read?id=${data[i]._id}"> ${ds} - ${data[i].title} </a>`
    blogListUl.appendChild(newLi)
  }
  console.log(blogListUl)
}

(function() {
  fetchGetAllTitles(handleResultGetAll)
})()
