export function getURLParams() {
    const queryParamsString = window.location.search.substr(1);
    const queryParams = queryParamsString
      .split('&')
      .reduce((accumulator, singleQueryParam) => {
        const [key, value] = singleQueryParam.split('=');
        accumulator[key] = decodeURIComponent(value);
        return accumulator;
      }, {});
    return queryParams
}