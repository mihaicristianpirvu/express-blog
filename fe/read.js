import {fetchRenderById} from "./api_calls.js"
import {getURLParams} from "./libs/urlparams.js"

console.log("[read.js] Client-side code running - read blogpost")

const readDiv = document.getElementById("my-read")
const editButton = document.getElementById("edit-button")
const blogPostId = getIdFromURL()

function getIdFromURL() {
    const queryParams = getURLParams();
    if (!("id" in queryParams)) {
        alert("no id for read")
        throw Error("no id for read!")
    }

    return queryParams["id"]
}

/* handles reads to the database */
function handleResultGet(data) {
    readDiv.innerHTML = data["data"]
    MathJax.typeset();
}

function buttonHandler() {
    document.addEventListener("keydown", (e) => {
        /* ctrl + e => goes to edit page */
        if (e.ctrlKey && e.keyCode == 69) {
            e.preventDefault()
            window.location.href = `write?id=${blogPostId}`
        }
    })
}

/* Main function of this code. */
(function() {
    editButton.setAttribute("href", `write?id=${blogPostId}`)
    fetchRenderById(blogPostId, handleResultGet)
    buttonHandler()

})()
