/* Handles fetches to DB */

/**
 * Generic prefix function. This allows us to use both locahost:port and some_host/blog/ with same api calls
 * @return The prefix (/blog) based on the browser's href.
 */
function getPrefix() {
    const url = new URL(window.location.href)
    if (url.port != "") {
        return ""
    }
    const splitPathname = url.pathname.split("/")
    return `/${splitPathname[1]}`
}

/**
 * Generic fetch function that also handles errors nicely
 * @param {string} url - The URL we are fetching to
 * @param {dict} options - The options of the fetch (get/post stuff)
 * @param {function(dict):void} handleResultFn - The callback that will do something on the frontend with
 * the server's json response
 */
function fetchAndCheckResponse(url, options, handleResultFn) {
    const prefix = getPrefix()
    url = `${prefix}${url}`
    console.log(`[fetchAndCheckResponse] url: '${url}' (prefix: ${prefix}). Options. Method: ${options["method"]}. ` +
                `Body: ${options["body"]}`)
    fetch(url, options)
        .then((response) => {
            if(!response.ok) {
                throw new Error("Request failed.")
            }
            if(response.status != 200) {
                throw new Error(`Requtest failed (but ok) with error code ${response.status}`)
            }

            response.json().then((data) => {
                console.log(`[fetchAndCheckResponse] Received a response. Keys: ${Object.keys(data)}`)
                handleResultFn(data)}
            )
        })
        .catch((error) => {
            document.write(`${error}. Call: ${url}. Options: ${JSON.stringify(options)}`);
            throw new Error(`Requtest failed (with catch) with error code ${error}`)
        })
}

export function fetchGetDBById(id, handleResultFn) {
    /* handle gets from db given one id */
    const inData = {id: id}
    const options = {
      method: "POST",
      body: JSON.stringify(inData),
      headers: {"Content-Type": "application/json"}
    }
    fetchAndCheckResponse("/get_post", options, handleResultFn)
}

export function fetchUpdateById(id, title, content, handleResultFn) {
    /* handles updates to the database given one id */
    const inData = {
        id: id,
        title: title,
        content: content
    }
    const options = {
      method: "POST",
      body: JSON.stringify(inData),
      headers: {"Content-Type": "application/json"}
    }
    fetchAndCheckResponse("/create_or_update", options, handleResultFn)
}

export function fetchGetAllTitles(handleResultFn) {
    const options = {
        method: "POST",
        body: JSON.stringify({}),
        headers: {"Content-Type": "application/json"}
    }
    fetchAndCheckResponse("/get_all", options, handleResultFn)
  }

  export function fetchRenderById(id, handleResultFn) {
    /* handle gets from db given one id */
    const inData = {id: id}
    const options = {
      method: "POST",
      body: JSON.stringify(inData),
      headers: {"Content-Type": "application/json"}
    }
    fetchAndCheckResponse("/render_post", options, handleResultFn)
}