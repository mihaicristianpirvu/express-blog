import level from "level";

var db = null;

/**
 * Create the LevelDB instance.
 * @constructor
 * @param {string} dbPath - The local path of the DB storage
 * @return The LevelDB object.
 */
export async function getDB(dbPath) {
    if (db != null) {
        throw new Error("DB already opened!");
    }

    db = new level.Level(dbPath, {keyEncoding: "binary", valueEncoding: "json"})
    while (db.status == "opening") {
        await new Promise(r => setTimeout(r, 500));
    }
    if(db.status != "open") {
        throw new Error(`Error opening, got status: ${db.status}`);
    }

    console.log(`Connected to db. (file: ${dbPath})`)
    return db;
}

/**
 * Makes a unique id for the DB
 * @param {int} length - The length of the id
 */
export function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


/**
 * Gets a DB post by id. Returns null if it doesn't exist.
 * @param {string} id - The ids we are looking for
 * @return - Returns the post, or null if the id doesn't exist.
 */
export async function getPostById(id) {
    /* generic function that gets the entire blog post given an id */
    console.log(`[getPostById] Requesting id ${id}`)

    try {
        const r = await db.get(id)
        return r
    }
    catch (error) {
        console.log(`[getPostById] Not found in db! Error: ${error}`)
        return null
    }
}
