import express from "express"
import path from "path";
import bodyParser from "body-parser";
import {marked} from "marked"

import {getDB, makeid, getPostById} from "./db.js"


/* Server stuff */
var app = express()
const feDir = path.resolve(path.dirname("")) + "/../fe"
app.use(express.static(feDir))
app.use(bodyParser.json())
console.log(`Front end directory ${feDir}`)
const port = process.argv.length == 3 ? parseInt(process.argv[2]) : 8080
const dbPath = "./db_blog"
var db = await getDB(dbPath)

var server = app.listen(port, function () {
    var host = server.address().address
    var port = server.address().port
    const hostAddr = host == "::" ? "localhost" : host
    console.log(`Example app listening at http://${hostAddr}:${port}`)
})

app.get("/", function (req, res) {
    console.log("[/] Called index page")
    res.sendFile("main.html", {root: feDir});
})

app.get("/write", function (req, res) {
    res.sendFile("write.html", {root: feDir});
})

app.get("/read", function (req, res) {
    res.sendFile("read.html", {root: feDir});
})

app.post("/create_or_update", async function(req, res) {
    /* TODO - keep track of all versions in the DB on the same id, but different timestamps. */
    const data = req.body;
    const updated = Date.now()
    var created;
    var newId;
    console.log(`[/create_or_update] Creating or updating id: ${data.id}. Title: ${data.title}. ` +
                `Length: ${data.content.length}`)
    if(!data.id) {
        newId = makeid(12)
        console.log(`No id provided, adding new entry at id '${newId}'`)
        created = updated;
    }
    else {
        const found = await getPostById(data.id)
        if(!found) {
            console.log(`Id ${data.id} provided, but not found in db.`)
            throw new Error();
        }
        newId = data.id;
        created = found.created
    }
    await db.put(newId, {title: data.title, content: data.content, created: created, updated: updated})
    res.send({id: newId})
})

app.post("/get_all", async function(req, res) {
    /* gets all the blog post titles from the database, excluding the text (for efficiency) */
    console.log("[/get_all] Getting all posts")
    var outArr = []
    for await (const [key, value] of db.iterator()) {
        outArr.push({_id: key.toString("utf8"), title: value["title"], timestamp: value["created"]})
    }
    if (outArr.length == 0) {
        res.status(201).send({error: "no values returned"})
        return
    }
    outArr = outArr.sort( (a, b) => (b.timestamp - a.timestamp) )
    res.send(outArr)
})

app.post("/get_post", async function(req, res) {
    console.log(`[/get_post] Requested id: ${req.body.id}`)
    const found = await getPostById(req.body.id)
    if (!found) {
        res.status(201).send({error: `not found id ${req.body.id}`})
        return
    }

    res.send({title: found["title"], content: found["content"], timestamp: found["created"]})
})

app.post("/render_post", async function(req, res) {
    /* SSR = Server side rendering */
    console.log(`[/render_post] Requested id: ${req.body.id}`)
    const found = await getPostById(req.body.id)
    if (!found) {
        res.status(404).send(`not found id ${req.body.id}`)
        return
    }
    const options = {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false
    };
    const ds = (new Date(found["created"])).toLocaleDateString("en-GB", options)
    const outData = marked(
`
<div class="blog_title"> ${found["title"]} </div>
<div class="blog_timestamp"> Posted at: ${ds} </div>
<div class="blog_content"> ${found["content"]} </div>
`)
    res.send({data: outData})
})

app.get("*", function(req, res) {
    console.log(`[*] Called wrong page: ${req}. Redirecting to index.`)
    res.sendFile("main.html", {root: feDir});
});
